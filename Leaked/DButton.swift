//
//  DButton.swift
//  Leaked
//
//  Created by Danilo Altheman on 13/02/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

@IBDesignable class DButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
}
