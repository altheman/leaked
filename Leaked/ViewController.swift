//
//  ViewController.swift
//  Leaked
//
//  Created by Danilo Altheman on 08/03/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//

import UIKit

// MARK: - State Class
class State {
    var name: String?
    var country: Country?
    init (name: String) {
        self.name = name
    }
}

// MARK: - Country Class
class Country {
    var name: String?

    // Solution?
    var state: State?

    init(name: String?) {
        self.name = name
    }
}

// MARK: - Controller class
class ViewController: UIViewController {
    // MARK: - Var & Const
    var buffer: [UInt8] = [UInt8]()
    var ramAmount: Int = 2
    var lastPoint: CGPoint!
    
    // MARK: - Outlets
    @IBOutlet weak var ramMemoryLabel: UILabel!
    @IBOutlet weak var drawImageView: UIImageView!

    // MARK: - App Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Actions
    @IBAction func leak(_ sender: Any) {
        let state = State(name: "Sao Paulo")
        let country = Country(name: "Brazil")
        state.country = country
        country.state = state
    }

    @IBAction func allocate(_ sender: Any) {
        allocateMemoryOfSize(numberOfMegaBytes: ramAmount)
    }
    
    @IBAction func increaseDecreateRAMamount(_ sender: UIStepper) {
        ramAmount = Int(sender.value)
        ramMemoryLabel.text = "\(ramAmount)MB"
        
        // Haptic Feedback
        UISelectionFeedbackGenerator().selectionChanged()
    }
    
    // MARK: - Functions
    func allocateMemoryOfSize(numberOfMegaBytes: Int) {
        print("Allocating \(numberOfMegaBytes)MB of memory")
        let megaBytes = 1048576
        let newBuffer = [UInt8](repeating: 0, count: numberOfMegaBytes * megaBytes)
        buffer += newBuffer
    }
}
