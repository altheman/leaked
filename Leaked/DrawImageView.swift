//
//  DrawImageView.swift
//  Leaked
//
//  Created by Danilo Altheman on 13/02/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

class DrawImageView: UIImageView {
    var swiped: Bool = true
    var lastTouchedPoint: CGPoint!
    
    // Computed property
    var noteLabel: UILabel = {
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 150))
        label.textAlignment = .center
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 17)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isUserInteractionEnabled = true
        self.layer.cornerRadius = 8
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 2
        
        // Long press over the draw area to clear the image
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(DrawImageView.clearDrawArea))
        longPress.minimumPressDuration = 2
        self.addGestureRecognizer(longPress)
        
        self.backgroundColor = .white
        
        noteLabel.text = "Draw here!\n\nLong Press to clear."
        addSubview(noteLabel)
        
        noteLabel.widthAnchor.constraint(equalToConstant: 250).isActive = true
        noteLabel.heightAnchor.constraint(equalToConstant: 250).isActive = true
        noteLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        noteLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    @objc func clearDrawArea() {
        self.image = UIImage()
        UINotificationFeedbackGenerator().notificationOccurred(.success)
        UIView.animate(withDuration: 0.5) {
            self.noteLabel.alpha = 1
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        noteLabel.alpha = 0
        if let touch = touches.first {
            lastTouchedPoint = touch.location(in: self)
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            drawLine(fromPoint: lastTouchedPoint, toPoint: currentPoint)
            lastTouchedPoint = currentPoint
        }
    }

    func drawLine(fromPoint: CGPoint, toPoint: CGPoint) {
        
        UIGraphicsBeginImageContext(self.frame.size)
        let context = UIGraphicsGetCurrentContext()
        image?.draw(in: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        
        context?.move(to: fromPoint)
        context?.addLine(to: toPoint)
        
        context?.setLineCap(CGLineCap.round)
        context?.setLineWidth(8)
        context?.setStrokeColor(red: 0, green: 0, blue: 0, alpha: 1.0)
        context?.setBlendMode(CGBlendMode.normal)
        context?.strokePath()
        
        self.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
}
