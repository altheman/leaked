//
//  NetworkViewController.swift
//  Leaked
//
//  Created by Danilo Altheman on 13/02/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

//https://picsum.photos/872/1634/?random
class NetworkViewController: UIViewController {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var downloadProgressView: UIProgressView!
    var downloadSession: URLSession!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getRandomImage()
    }
    
    @IBAction func refreshImage(_ sender: Any) {
        getRandomImage()
    }
    
    func getRandomImage() {
        guard let destinationURL = URL(string: "https://picsum.photos/872/1634/?random") else {
            let alertController = UIAlertController(title: "Error", message: "Cannot Connect to URL", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
            return

        }
        
        let downloadConfiguration = URLSessionConfiguration.background(withIdentifier: "com.apple.developeracademy.Leaked.downloadImage")
        downloadSession = URLSession(configuration: downloadConfiguration, delegate: self, delegateQueue: OperationQueue())
        let downloadTask = downloadSession.downloadTask(with: destinationURL)
        downloadTask.resume()
        self.downloadProgressView.alpha = 1
        self.downloadProgressView.progress = 0.01
    }
}

extension NetworkViewController: URLSessionDownloadDelegate {

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        if totalBytesExpectedToWrite > 0 {
            let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
            DispatchQueue.main.async {
                self.downloadProgressView.progress = progress
            }
        }
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.7, animations: {
                self.downloadProgressView.alpha = 0
                self.downloadSession = nil
            })
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        if let data = try? Data(contentsOf: location), let image = UIImage(data: data) {
            DispatchQueue.main.async {
                self.mainImageView.image = image
            }
        }
    }
}
